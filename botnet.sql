-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 02 Paź 2014, 00:48
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `botnet`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `op` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Zrzut danych tabeli `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `email`, `op`) VALUES
(1, 'RosheD', 'test', 'aqua18@o2.pl', 1),
(2, 'test', 'test', '', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `Img` varchar(999) NOT NULL,
  `Imie` varchar(40) NOT NULL,
  `Nazwisko` varchar(40) NOT NULL,
  `Lat` int(3) NOT NULL,
  `Pesel` varchar(20) NOT NULL,
  `Zarobki` int(7) NOT NULL,
  `Tel` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `Img`, `Imie`, `Nazwisko`, `Lat`, `Pesel`, `Zarobki`, `Tel`) VALUES
(1, 'http://e.egoisci.pl/eg915/dd1d7a610016f36e47be11b8', 'Konrad', 'Koczewski', 29, '8320019702', 9999, '56154232'),
(2, 'http://fryzjerstwozpasja.blox.pl/resource/LeonardoDicaprioleonardodicaprio19374229900902_250x250.jpg', 'Jan', 'Blachewski', 31, '8320019701', 99999, '523210252'),
(21, 'https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-xfp1/v/t1.0-9/10523140_771534136244142_5052620227742617913_n.jpg?oh=889ad111cb22a815dad7d6b914e8b91d&oe=54B9B5B0&__gda__=1421796153_8e9f41ef60d7a31d0ff67db692b518e1', 'Piotr', 'Kauczor', 71, '9701200891', 5465465, '7979456');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
